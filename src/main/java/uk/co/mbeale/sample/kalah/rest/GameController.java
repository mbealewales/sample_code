package uk.co.mbeale.sample.kalah.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uk.co.mbeale.sample.kalah.GameManager;
import uk.co.mbeale.sample.kalah.data.Game;
import uk.co.mbeale.sample.kalah.data.GameBoardStatus;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(produces="application/json")
@CrossOrigin(origins = "*")
public class GameController {

  @Autowired
  private GameManager gameManager;

  @PostMapping(path="/games")
  public ResponseEntity<Game> postStartGame() {
    return new ResponseEntity<>(gameManager.createGame(), HttpStatus.CREATED);
  }

  @PutMapping(path="/games/{gameId}/pits/{pitId}")
  public ResponseEntity<GameBoardStatus> putMakeMove(
      @PathVariable("gameId") Integer gameNumber, @PathVariable("pitId") Integer pitNumber,
      HttpSession httpSession) {

    final Game game = gameManager.getGame(gameNumber);
    game.makeMove(pitNumber - 1 , httpSession.getId());

    return new ResponseEntity<>(new GameBoardStatus(), HttpStatus.OK);
  }
}
