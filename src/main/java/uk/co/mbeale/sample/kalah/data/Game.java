package uk.co.mbeale.sample.kalah.data;

import uk.co.mbeale.sample.kalah.*;

import java.util.HashMap;
import java.util.Map;


public class Game {

  private GameState gameState = GameState.Created;
  private Player gameWinner;

  private GameBoard gameBoard;

  private Object lock = new Object();

  private Map<String, Player> userIdentiferToPlayer = new HashMap<>();
  private int gameNumber;

  private Player expectedNextPlayer;

  private String uri;

  public Game(int gameNumber, GameBoard gameBoard) {
    this.gameNumber = gameNumber;
    this.gameBoard = gameBoard;
    uri = "HELLO";
  }

  private void validateIsNonEmptyPitOwnedByPlayer(Integer pitNumber, Player player) {
    if (!this.gameBoard.playerOwnsPit(player, pitNumber)) {
      throw new WrongPlayerException();
    } else if (this.gameBoard.isEmptyPit(pitNumber)) {
      throw new InvalidMoveException();
    }
  }

  private void validateIsExpectedToPlay(Player player) {
    if (!player.equals(expectedNextPlayer)) {
      throw new WrongPlayerException();
    }
  }

  private void validateGameState() {
    if (this.gameState.equals(GameState.Completed)) {
      throw new GameCompletedException();
    }
  }

  private boolean playerHasUsedAllSeeds(Player player) {
    for (int pitIdx = this.gameBoard.getLeftPitForPlayer(player); pitIdx <= this.gameBoard.getRightPitForPlayer(player); pitIdx++) {
      if (this.gameBoard.pitNotEmpty(pitIdx)) {
        return false;
      }
    }
    return true;
  }

  private void calculateWinner() {
    if (this.gameBoard.seedsInPit(this.gameBoard.getHousePitForPlayer(Player.Player1)) >
        this.gameBoard.seedsInPit(this.gameBoard.getHousePitForPlayer(Player.Player2))) {
      this.gameWinner = Player.Player1;
    } else {
      this.gameWinner = Player.Player2;
    }
  }

  private void emptySeedsForPlayer(Player player) {
    for (int pitIdx = this.gameBoard.getLeftPitForPlayer(player); pitIdx <= this.gameBoard.getRightPitForPlayer(player); pitIdx++) {
      this.gameBoard.moveSeedsToHousePitForPlayerFromPit(player, pitIdx);
    }
  }

  /**
   * Distribute the seeds from the player's indicated pit number.
   * Determine the player who will make the next move AND whether the
   * game has completed.
   * @param player the player making the move.
   * @param pitNumber the player's pit that the seeds are moved from.
   */
  private void distributeSeedsForPlayerFromPit(Player player, Integer pitNumber) {
    int nextPit = pitNumber + 1;
    int seedsInHand = this.gameBoard.pickUpSeedsFromPit(pitNumber);

    int destinationPit = 0;
    while (seedsInHand > 0) {
      destinationPit = nextPit;
      this.gameBoard.dropSeedInPit(nextPit++);

      if (nextPit == this.gameBoard.getOpponentHousePitForPlayer(player)) {
        nextPit++;
      }
      if (nextPit >= GameBoard.BOARD_SIZE) {
        nextPit = 0;
      }
      seedsInHand--;
    }

    if (destinationPit == this.gameBoard.getHousePitForPlayer(player)) {
      this.expectedNextPlayer = player;
    }
    else {
      if (this.gameBoard.playerOwnsPit(player, destinationPit) && (this.gameBoard.seedsInPit(destinationPit) == 1)) {
        this.gameBoard.moveSeedsToHousePitForPlayerFromPit(player, destinationPit);
        this.gameBoard.playerCapturesSeedsInPit(player, destinationPit);
      }
      this.expectedNextPlayer =  player.opponent();
    }
    if (playerHasUsedAllSeeds(player)) {
      setGameState(GameState.Completed);
      emptySeedsForPlayer(player.opponent());
      calculateWinner();
    }
  }

  public GameState getGameState() {
    return this.gameState;
  }

  private void setGameState(GameState gameState) {
    this.gameState = gameState;
  }

  public Player getGameWinner() {
    return this.gameWinner;
  }

  protected void associateUserIdentifierWithPlayer(String userIdentifier, Player player) {
    this.userIdentiferToPlayer.put(userIdentifier, player);
  }

  public synchronized void makeMove(Integer pitNumber, String userIdentifier) {

    validateGameState();

    final Player player;

    try {
      if (this.userIdentiferToPlayer.containsKey(userIdentifier)) {

        player = this.userIdentiferToPlayer.get(userIdentifier);
        validateIsExpectedToPlay(player);
        validateIsNonEmptyPitOwnedByPlayer(pitNumber, player);

      } else if (this.userIdentiferToPlayer.size() == 0) {

        player = this.gameBoard.determinePlayerFromPit(pitNumber);
        associateUserIdentifierWithPlayer(userIdentifier, player);

      } else if (this.userIdentiferToPlayer.size() == 1) {

        player = this.gameBoard.determinePlayerFromPit(pitNumber);
        validateIsExpectedToPlay(player);
        associateUserIdentifierWithPlayer(userIdentifier, player);

      } else {

        throw new WrongNumberOfPlayersException();

      }
    } catch (IllegalArgumentException iae) {
      throw new InvalidMoveException(iae);
    }
    setGameState(GameState.Started);
    distributeSeedsForPlayerFromPit(player, pitNumber);
  }

  public String getId() {
    return String.valueOf(this.gameNumber);
  }

  public String getUri() {
    return uri;
  }
}
