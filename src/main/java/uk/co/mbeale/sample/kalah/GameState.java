package uk.co.mbeale.sample.kalah;

public enum GameState {
  Created,
  Started,
  Completed
}
