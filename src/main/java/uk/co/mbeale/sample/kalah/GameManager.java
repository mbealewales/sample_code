package uk.co.mbeale.sample.kalah;

import org.springframework.stereotype.Component;
import uk.co.mbeale.sample.kalah.data.Game;

import java.util.HashMap;
import java.util.Map;

@Component
public class GameManager {
  private Integer nextGameNumber = 0;

  private Map<Integer, Game>  games = new HashMap<>();

  public synchronized Game createGame() {

    final Game game = new Game(this.nextGameNumber, new GameBoard());
    this.games.put(this.nextGameNumber, game);
    return game;
  }

  public synchronized Game getGame(Integer gameNumber) {
    if (!this.games.containsKey(gameNumber)) {
      throw new InvalidGameException(gameNumber);
    }
    return this.games.get(gameNumber);
  }
}
