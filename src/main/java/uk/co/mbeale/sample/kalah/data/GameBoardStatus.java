package uk.co.mbeale.sample.kalah.data;

import java.util.HashMap;
import java.util.Map;

public class GameBoardStatus extends Game {
  private Map<String, String> board;

  public GameBoardStatus() {
    super(1);

    board = new HashMap<>();
    board.put("1", "20");
    board.put("2", "12");
  }

  public Map<String, String> getBoard() {
    return board;
  }
}
