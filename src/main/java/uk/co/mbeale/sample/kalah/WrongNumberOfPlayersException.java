package uk.co.mbeale.sample.kalah;

public class WrongNumberOfPlayersException extends RuntimeException {
  public WrongNumberOfPlayersException() {
    super("There are already players in the game");
  }
}
