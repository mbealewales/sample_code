package uk.co.mbeale.sample.kalah;

public class GameCompletedException extends RuntimeException {
  public GameCompletedException() {
    super("Cannot play move as game has already completed.");
  }
}
