package uk.co.mbeale.sample.kalah;

public class InvalidMoveException extends RuntimeException {
  public InvalidMoveException(Exception ex) {
    super(ex);
  }
  public InvalidMoveException() {
    super("Illegal Move made");
  }
}
