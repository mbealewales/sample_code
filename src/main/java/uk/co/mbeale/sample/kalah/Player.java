package uk.co.mbeale.sample.kalah;

public enum Player {
  Player1("Kalah Player 1"),
  Player2("Kalah Player 2");


  private String playerName;

  Player(String playerName) {
    this.playerName = playerName;
  }

  public String getPlayerName() {
    return this.playerName;
  }

  public Player opponent() {
    return this.equals(Player1) ? Player2 : Player1;
  }
}
