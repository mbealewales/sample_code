package uk.co.mbeale.sample.kalah;

import java.util.HashMap;
import java.util.Map;

public class GameBoard {
  private static final int INITIAL_SEEDS_IN_PIT = 6;

  public final class PlayerOnBoardDetails {
    private int housePit, leftPit, rightPit, opponentHousePit;

    private PlayerOnBoardDetails(int housePit, int leftPit, int rightPit, int opponentHousePit) {
      this.housePit = housePit;
      this.leftPit = leftPit;
      this.rightPit = rightPit;
      this.opponentHousePit = opponentHousePit;
    }
  }

  public static final int BOARD_SIZE = 14;

  private Map<Player, PlayerOnBoardDetails> playersOnBoard = new HashMap<>();

  private int[] pits;

  private void initPlayersOnBoardDetail() {
    this.playersOnBoard.put(Player.Player1,
        new PlayerOnBoardDetails(6, 0, 5, 13));
    this.playersOnBoard.put(Player.Player2,
        new PlayerOnBoardDetails(13, 7, 12, 6));
  }

  public GameBoard() {
    initPlayersOnBoardDetail();

    this.pits = new int[BOARD_SIZE];
    for (int idx = 0; idx < BOARD_SIZE; idx++) {
      this.pits[idx] = INITIAL_SEEDS_IN_PIT;
    }
    this.pits[getHousePitForPlayer(Player.Player1)] = 0;
    this.pits[getHousePitForPlayer(Player.Player2)] = 0;

  }

  public GameBoard(int[] pitState) {
    if ((pitState == null) || (pitState.length != BOARD_SIZE)) {
      throw new IllegalArgumentException("Wrong board size");
    }
    initPlayersOnBoardDetail();

    this.pits = pitState;
  }

  public boolean playerOwnsPit(Player player, int pitNumber) {
    return ((pitNumber >= getLeftPitForPlayer(player)) && (pitNumber <= getRightPitForPlayer(player)));
  }

  public Player determinePlayerFromPit(Integer pitNumber) {
    if (playerOwnsPit(Player.Player1, pitNumber)) {
      return Player.Player1;
    } else if (playerOwnsPit(Player.Player2, pitNumber)) {
      return Player.Player2;
    } else {
      throw new IllegalArgumentException("No player owns specified pit.");
    }
  }

  public int getLeftPitForPlayer(Player player) {
    return this.playersOnBoard.get(player).leftPit;
  }
  public int getRightPitForPlayer(Player player) {
    return this.playersOnBoard.get(player).rightPit;
  }
  public int getHousePitForPlayer(Player player) {
    return this.playersOnBoard.get(player).housePit;
  }
  public int getOpponentHousePitForPlayer(Player player) {
    return this.playersOnBoard.get(player).opponentHousePit;
  }
  public int getOppositePit(int pitNumber) {
    final Player player = determinePlayerFromPit(pitNumber);
    if (Player.Player1.equals(player)) {
      return getRightPitForPlayer(Player.Player2) - pitNumber;
    } else {
      return getLeftPitForPlayer(Player.Player2) - pitNumber + getRightPitForPlayer(Player.Player1);
    }
  }

  public boolean isEmptyPit(int pitNumber) {
    return this.pits[pitNumber] == 0;
  }
  public boolean pitNotEmpty(int pitNumber) {
    return this.pits[pitNumber] > 0;
  }
  public int seedsInPit(int pitNumber) {
    return this.pits[pitNumber];
  }
  public int pickUpSeedsFromPit(int pitNumber) {
    int seedCount = this.pits[pitNumber];
    this.pits[pitNumber] = 0;

    return seedCount;
  }
  public void moveSeedsToHousePitForPlayerFromPit(Player player, int sourcePit) {
    int seedsToTransfer = this.pits[sourcePit];
    this.pits[sourcePit] = 0;
    this.pits[getHousePitForPlayer(player)] += seedsToTransfer;
  }
  public void dropSeedInPit(int pitNumber) {
    this.pits[pitNumber]++;
  }

  public void playerCapturesSeedsInPit(Player player, int pitNumber) {
    int oppositePit = getOppositePit(pitNumber);

    this.pits[getHousePitForPlayer(player)] += this.pits[oppositePit];
    this.pits[oppositePit] = 0;
  }
}
