package uk.co.mbeale.sample.kalah;

public class WrongPlayerException extends RuntimeException {
  public WrongPlayerException() {
    super("Player played out sequence");
  }
}
