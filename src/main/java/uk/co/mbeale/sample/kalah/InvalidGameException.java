package uk.co.mbeale.sample.kalah;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidGameException extends RuntimeException {
  public InvalidGameException(Integer gameNumber) {
    super("No such game " + gameNumber);
  }
}
