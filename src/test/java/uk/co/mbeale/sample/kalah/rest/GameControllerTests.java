package uk.co.mbeale.sample.kalah.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GameController.class, secure = false)
public class GameControllerTests {

  private static final String CREATE_URI = "/games";
  private static final String MAKE_VALID_MOVE_URI = "/games/1234/pits/xyz";
  private static final String MAKE_INVALID_MOVE_URI = "/games/134/pits/xyz";

  @Autowired
  private MockMvc mockMvc;

  private String sendCreate(RequestBuilder requestBuilder) throws Exception {
    final MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    final MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    return response.getContentAsString();
  }

  private String sendMakeMoveExpectResponseCode(RequestBuilder requestBuilder,
      HttpStatus expectedResponseStatus) throws Exception {
    final MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    final MockHttpServletResponse response = result.getResponse();

    assertEquals(expectedResponseStatus.value(), response.getStatus());
    return response.getContentAsString();
  }

  @Test
  public void createGame() throws Exception {
    final RequestBuilder requestBuilder = MockMvcRequestBuilders.
        post(CREATE_URI).accept(MediaType.APPLICATION_JSON).
        contentType(MediaType.APPLICATION_JSON);

    final String game1JSON = sendCreate(requestBuilder);
    final String game2JSON = sendCreate(requestBuilder);
  }

  @Test
  public void makeValidMove() throws Exception {
    final RequestBuilder requestBuilder = MockMvcRequestBuilders.
        put(MAKE_VALID_MOVE_URI).accept(MediaType.APPLICATION_JSON).
        contentType(MediaType.APPLICATION_JSON);

    sendMakeMoveExpectResponseCode(requestBuilder, HttpStatus.OK);
  }

  @Test
  public void makeInValidMove() throws Exception {
    final RequestBuilder requestBuilder = MockMvcRequestBuilders.
        put(MAKE_INVALID_MOVE_URI).accept(MediaType.APPLICATION_JSON).
        contentType(MediaType.APPLICATION_JSON);

    sendMakeMoveExpectResponseCode(requestBuilder, HttpStatus.NOT_FOUND);
  }
}
