package uk.co.mbeale.sample.kalah;

import org.junit.Test;
import uk.co.mbeale.sample.kalah.data.Game;

import static org.junit.Assert.assertEquals;

public class GameTests {
  private static final int GAME_ID = 43854;

  @Test
  public void testInitialGameStateAndNumber() {
    final Game testable = new Game(GAME_ID, new GameBoard());

    assertEquals("Wrong initial game state", GameState.Created, testable.getGameState());
    assertEquals("Wrong game number", GAME_ID, testable.getId());
  }

}
