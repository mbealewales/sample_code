package uk.co.mbeale.sample.kalah;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameBoardTests {

  private GameBoard testable;

  @Before
  public void setupTest() {
    int initialGameBoardState[] = {0, 4, 5, 2, 3, 2, 2, 4, 1, 0, 2, 0, 6, 5};
    testable = new GameBoard(initialGameBoardState);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testWrongBoardSize() {
    int badGameBoardState[] = {0, 4, 5, 2, 3, 2, 0, 4, 2, 0, 6, 0};
    new GameBoard(badGameBoardState);
  }
  @Test(expected = IllegalArgumentException.class)
  public void testMissingInitialBoardState() {
    new GameBoard(null);
  }

  @Test
  public void testPlayerOwnsPit() {
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player1, 0));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player1, 1));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player1, 2));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player1, 3));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player1, 4));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player1, 5));

    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player2, 7));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player2, 8));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player2, 9));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player2, 10));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player2, 11));
    assertTrue("Player does not own expected pit", testable.playerOwnsPit(Player.Player2, 12));

    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player2, 0));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player2, 1));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player2, 2));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player2, 3));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player2, 4));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player2, 5));

    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player1, 7));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player1, 8));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player1, 9));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player1, 10));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player1, 11));
    assertFalse("Player owns unexpected pit", testable.playerOwnsPit(Player.Player1, 12));
  }

  @Test
  public void testDeterminePlayerFromPit() {
    assertEquals("Wrong player", Player.Player1, testable.determinePlayerFromPit(0));
    assertEquals("Wrong player", Player.Player1, testable.determinePlayerFromPit(1));
    assertEquals("Wrong player", Player.Player1, testable.determinePlayerFromPit(2));
    assertEquals("Wrong player", Player.Player1, testable.determinePlayerFromPit(3));
    assertEquals("Wrong player", Player.Player1, testable.determinePlayerFromPit(4));
    assertEquals("Wrong player", Player.Player1, testable.determinePlayerFromPit(5));

    assertEquals("Wrong player", Player.Player2, testable.determinePlayerFromPit(7));
    assertEquals("Wrong player", Player.Player2, testable.determinePlayerFromPit(8));
    assertEquals("Wrong player", Player.Player2, testable.determinePlayerFromPit(9));
    assertEquals("Wrong player", Player.Player2, testable.determinePlayerFromPit(10));
    assertEquals("Wrong player", Player.Player2, testable.determinePlayerFromPit(11));
    assertEquals("Wrong player", Player.Player2, testable.determinePlayerFromPit(12));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testNoPlayerOwnsBadPit() {
    testable.determinePlayerFromPit(15);
  }

  @Test
  public void testPitInformationForPlayer() {
    assertEquals("Wrong house pit", 6, testable.getHousePitForPlayer(Player.Player1));
    assertEquals("Wrong left pit", 0, testable.getLeftPitForPlayer(Player.Player1));
    assertEquals("Wrong right pit", 5, testable.getRightPitForPlayer(Player.Player1));
    assertEquals("Wrong opponent house pit", 13, testable.getOpponentHousePitForPlayer(Player.Player1));

    assertEquals("Wrong house pit", 13, testable.getHousePitForPlayer(Player.Player2));
    assertEquals("Wrong left pit", 7, testable.getLeftPitForPlayer(Player.Player2));
    assertEquals("Wrong right pit", 12, testable.getRightPitForPlayer(Player.Player2));
    assertEquals("Wrong opponent house pit", 6, testable.getOpponentHousePitForPlayer(Player.Player2));
  }

  @Test
  public void testGetOppositePit() {
    assertEquals("Wrong opposite pit", 12, testable.getOppositePit(0));
    assertEquals("Wrong opposite pit", 11, testable.getOppositePit(1));
    assertEquals("Wrong opposite pit", 10, testable.getOppositePit(2));
    assertEquals("Wrong opposite pit", 9, testable.getOppositePit(3));
    assertEquals("Wrong opposite pit", 8, testable.getOppositePit(4));
    assertEquals("Wrong opposite pit", 7, testable.getOppositePit(5));

    assertEquals("Wrong opposite pit", 5, testable.getOppositePit(7));
    assertEquals("Wrong opposite pit", 4, testable.getOppositePit(8));
    assertEquals("Wrong opposite pit", 3, testable.getOppositePit(9));
    assertEquals("Wrong opposite pit", 2, testable.getOppositePit(10));
    assertEquals("Wrong opposite pit", 1, testable.getOppositePit(11));
    assertEquals("Wrong opposite pit", 0, testable.getOppositePit(12));
  }

  @Test
  public void testIsEmptyPit() {
    assertTrue("Pit should be empty", testable.isEmptyPit(0));
    assertFalse("Pit should not be empty", testable.isEmptyPit(1));
  }

  @Test
  public void testPitNotEmpty() {
    assertTrue("Pit should not be empty", testable.pitNotEmpty(1));
    assertFalse("Pit should be empty", testable.pitNotEmpty(0));
  }

  @Test
  public void testSeedsInPit() {
    assertEquals("Wrong seed count", 5, testable.seedsInPit(2));
    assertEquals("Wrong seed count", 1, testable.seedsInPit(8));
  }

  @Test
  public void testPickUpSeedsUpFromPit() {
    assertEquals("Picked up wrong number of seeds", 5, testable.pickUpSeedsFromPit(2));
    assertEquals("Picked up wrong number of seeds", 1, testable.pickUpSeedsFromPit(8));

    assertEquals("Wrong seed count", 0, testable.seedsInPit(2));
    assertEquals("Wrong seed count", 0, testable.seedsInPit(8));
  }

  @Test
  public void testMoveSeedsToHousePitForPlayerFromPit() {
    testable.moveSeedsToHousePitForPlayerFromPit(Player.Player1, 1);
    testable.moveSeedsToHousePitForPlayerFromPit(Player.Player2, 10);

    assertEquals("Wrong seed count", 0, testable.seedsInPit(1));
    assertEquals("Wrong seed count", 0, testable.seedsInPit(10));

    assertEquals("Wrong seed count", 6, testable.seedsInPit(6));
    assertEquals("Wrong seed count", 7, testable.seedsInPit(13));
  }

  @Test
  public void testDropSeedInPit() {
    testable.dropSeedInPit(7);
    assertEquals("Wrong seed count", 5, testable.seedsInPit(7));
  }

  @Test
  public void testPlayerCapturesSeedsInPit() {
    testable.playerCapturesSeedsInPit(Player.Player1, 2);
    assertEquals("Wrong seed count", 0, testable.seedsInPit(10));
    assertEquals("Wrong seed count", 4, testable.seedsInPit(6));
  }

}
